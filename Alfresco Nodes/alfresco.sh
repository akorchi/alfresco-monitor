#!/bin/bash 
ALF_HOME="/opt/alfresco" 
APPSERVER="/opt/tomcat" 
#Stop also le process soffice 
SHUTDOWN_OOO=false 

export JAVA_HOME="/opt/jdk1.8" 
# Étendre le nombre max de fichiers ouvert par le processus Alfresco. 
#ulimit -n 4096 

cd "$ALF_HOME/logs" # Facultatif si le log4j est bien configuré 

export JAVA_OPTS="-server  -Xss1024k" 
export JAVA_OPTS="${JAVA_OPTS} -Dalfresco.home=${ALF_HOME} -Dcom.sun.management.jmxremote" 
export JAVA_OPTS="${JAVA_OPTS} -Djava.rmi.server.hostname=ld3gat001i"
export JAVA_OPTS="${JAVA_OPTS} -Duser.language=fr -Duser.country=FR"

pid=$(pgrep -f org.apache.catalina.startup.Bootstrap) 


#Script to run logstash client when Alfresco/tomcat start
export RUN_ELASTICSEARCH=/opt/alfresco/logstash-elasticsearch
export TOMCAT_POST_INIT_SCRIPT="$RUN_ELASTICSEARCH/run_logstash.sh"

if [ "$1" = "start" ]; then 
  if [ "$pid" ]; then 
    echo "Tomcat is already running ($pid)" 
  else 
    # Configuration pour 6Go RAM 
    export JAVA_OPTS="${JAVA_OPTS} -Xms5632m -Xmx5632m -XX:NewRatio=1" 

    # Protège de l'erreur "Can't connect to X11 window server using ':0.0' as the value of the DISPLAY variable." 
    export JAVA_OPTS="${JAVA_OPTS} -Djava.awt.headless=true" 

    "${APPSERVER}/bin/startup.sh" 
  fi 
elif [ "$1" = "stop" ]; then 

#Stop elasticsearch when Alfresco stop
$RUN_ELASTICSEARCH/run_logstash.sh stop

  if [ "$pid" ]; then 
    "${APPSERVER}/bin/shutdown.sh" 
    echo -n "(waiting" 
    wait=0 
    while [ "$wait" -lt "12" ]; do 
      wait=$(($wait + 1)) 
      if [ -f /proc/$pid/exe ]; then 
        echo -n "." 
        sleep 15s 
      else 
        break 
      fi 
      if [ "$wait" -gt "10" ]; then 
        echo -n "!" 
        kill $pid 
        sleep 5s 
      fi 
    done 
    echo ")" 
  else 
    echo "Tomcat not running" 
  fi 
fi 

if $SHUTDOWN_OOO ; then 
  pidSoffice=$(pgrep soffice) 
  if [ "$pidSoffice" ]; then 
    echo "Kill soffice : $pidSoffice" 
    pkill soffice 
  fi 

  sleep 5s 
 
  pidSoffice=$(pgrep soffice) 
  if [ "$pidSoffice" ]; then 
    echo "Force Kill soffice : \n$pidSoffice" 
    pkill -9 soffice 
    echo "soffice process killed" 
  fi 
fi 
